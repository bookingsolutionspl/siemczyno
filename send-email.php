<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

$name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
$email = filter_var($_POST["email"], FILTER_SANITIZE_STRING);
$phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
$message = filter_var($_POST["message"], FILTER_SANITIZE_STRING);
$dateRange = filter_var($_POST["dateRange"], FILTER_SANITIZE_STRING);

if ($name != '' && $email != '' && $phone != '' && $message != '') {

    $messageBody = '
        Nadawca: ' . $name  . ', <a href="mailto:' . $email . '">' . $email . '</a></br>
        Telefon: ' . '<a href="tel:' . $phone . '">' .$phone . '</a></br>
        Data: ' . $dateRange . '</br>
        Treść wiadomości:</br>' . $message . '</br></br>--</br>
        Ta wiadomość została wysłana przez formularz kontaktowy na stronie Pałac Siemczyno ' . '<a href="' . $_SERVER['HTTP_HOST'] . '">' . $_SERVER['HTTP_HOST'] . '</a>'
    ;

    $mail = new PHPMailer;
    $mail->isMail();
    // $mail->isSMTP();
    // $mail->SMTPDebug = 2;
    // $mail->Host = "smtp.mailtrap.io"; 
    // $mail->Port = "2525";
    // $mail->SMTPSecure = 'tls';
    // $mail->SMTPAuth = true;
    // $mail->Username = "d82374f4cbfb31";
    // $mail->Password = "37643dcc771410";
    $mail->CharSet = 'UTF-8';
    $mail->setFrom("biuro@palacsiemczyno.pl", "Pałac Siemczyno");
    $mail->addAddress("biuro@palacsiemczyno.pl");
    $mail->Subject = 'Nowa wiadomość ze strony internetowej - ' . $name;
    $mail->msgHTML($messageBody);
    $mail->AltBody = 'HTML not supported';

    try {
        $mail->send();
    }
    catch(\Exception $e) {
        http_response_code(500);
        die();
    }

    http_response_code(200);
    die();
}
http_response_code(500);

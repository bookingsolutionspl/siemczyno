const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');

module.exports = {
    entry: {
        main: './src/app/app.js',
        images: './src/app/images.js',
        styles: './src/app/styles.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    watchOptions: {
        ignored: [
            './node_modules/',
            './dist/',
        ],
    },
    plugins: [
        new MiniCssExtractPlugin(),
    ],
    stats: {
        children: true,
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src/app'),
                // loader: 'babel-loader',
            },
            {
                test: /\.scss$/,
                include: path.resolve(__dirname, 'src/styles'),
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                          url: false,
                          sourceMap: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    require('postcss-import'),
                                    require('autoprefixer'),
                                ]
                            }
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("dart-sass"),
                            sourceMap: true,
                            sassOptions: {
                                precision: 9,
                                outputStyle: "compressed",
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|webp)$/i,
                include: path.resolve(__dirname, 'src/images'),
                // oneOf: [
                //     {
                //         resourceQuery: /thumbnail/,
                //         use: [
                //             {
                //                 loader: 'webpack-image-srcset-loader',
                //                 options: {
                //                     sizes: ["50w"],
                //                 },
                //             },
                //             {
                //                 loader: 'file-loader',
                //                 options: {
                //                     name: '[path][name]-thumbnail.[ext]',
                //                 },
                //             },
                //             {
                //                 loader: 'webpack-image-resize-loader',
                //             },
                //         ]
                //     },
                //     {
                //         use: [
                //             {
                //                 loader: 'file-loader',
                //                 options: {
                //                     name: '[path][name].[ext]',
                //                 },
                //             },
                //             {
                //                 loader: ImageMinimizerPlugin.loader,
                //                 options: {
                //                     deleteOriginalAssets: false,
                //                     filename: '[path][name].webp',
                //                     minimizerOptions: {
                //                         plugins: ['imagemin-webp'],
                //                     },
                //                 },
                //             },
                //         ]
                //     },
                // ],
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src',
                        },
                    },
                    {
                        loader: ImageMinimizerPlugin.loader,
                        options: {
                            deleteOriginalAssets: false,
                            // filename: '[path][name].webp',
                            minimizerOptions: {
                                plugins: ['imagemin-webp'],
                            },
                        },
                    },
                ]
            },
            {
                test: /\.(gif|svg)$/i,
                include: path.resolve(__dirname, 'src/images'),
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: 'src',
                        },
                    },
                ],
            },
        ]
    }
}

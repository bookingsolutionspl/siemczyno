<?php
/**
 * Template Name: Strona główna
 */

get_header();
$fields = get_fields();
get_template_part('template-parts/slider', 'events');
?>

<div class="bg-image overflow-hidden">
    <div class="image">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
    <div class="section section-p-big pb-0">
        <div class="container">
            <div class="narrow">
                <div class="logo-home-page mb-5 entry">
                    <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                </div>
                <div class="title-wrapper text-center mb-5 entry">
                    <h2 class="title-primary line-decoration h2"><?= $fields['sekcja_1']['tytul'] ?></h2>
                </div>
                <div class="mb-5 entry">
                    <div class="content text-center narrow mb-5 h5-standard entry">
                        <?= $fields['sekcja_1']['opis'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="dot-container">
            <div class="image-wrapper">
                <?= getImageSvgSrc("/dist/images/icons/features/dot.svg") ?>
            </div>
        </div>
    </div>
</div>

<div class="section scrollTo pt-0 pb-0">
    <div class="container">
        <div class="title-wrapper text-center mb-6 entry">
            <h2 class="title-primary h2"><?= $fields['podstrony']['opis'] ?></h2>
        </div>
        <div class="rooms-box-main mb-6">
            <div class="row-1">

                <?php foreach ($fields['podstrony'] as $key => $podstrona) {
                    if (strpos($key, 'podstrona_') !== false) {?>
                    <div class="item item-secondary entry">
                        <div class="image-wrapper">
                            <div class="image">
                                <?= getImage($podstrona['obraz'], 'true') ?>
                            </div>
                        </div>
                        <div class="content-wrapper">
                            <div class="content">
                                <h2 class="title-primary text-white h3 mb-auto"><?= $podstrona['tytul'] ?></h2>
                                
                                <div class="button-wrapper">
                                    <hr class="mb-4 hr-white">
                                    <a href="<?= $podstrona['link']['url'] ?>" class="btn btn-third third-white"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } } ?>

            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="bg-image-parallax plain-dark">
        <div class="parallax-wrapper">
            <img class="lazy img-parallax" data-src="<?= $fields['sekcja_z_obrazem']['obraz'] ?>" data-speed="1" alt="">
        </div>
    </div>
    <div class="container">
        <div class="third-box narrow">
            <div class="title-wrapper text-center mb-5">
                <h2 class="title-primary text-white h2"><?= $fields['sekcja_z_obrazem']['tytul'] ?></h2>
            </div>
            <div class="content text-white text-center h5-standard">
                <?= $fields['sekcja_z_obrazem']['opis'] ?>
            </div>
        </div>
        <div class="line-box">
            <div class="line-wrapper">
                <div class="decorative-line"></div>
            </div>
        </div>
    </div>
</div>

<div class="section pb-0">
    <div class="map lazy-map"></div>   
</div>

<?php get_template_part('template-parts/coupon', ''); ?>


<?php
get_footer();
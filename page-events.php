<?php

/**
 * Template Name: Wydarzenia
 */

get_header();
$fields = get_fields();

$taxQuery = [];

if (isset($_GET['category'])) {
    array_push($taxQuery, [
        'taxonomy' => 'events_type',
        'field' => 'slug',
        'terms' => $_GET['category'] ?: '',
    ]);
}

$args = [
    'post_type' => 'events_group',
    'tax_query' => $taxQuery,
    'meta_key'  => 'data',
	'orderby'   => 'meta_value',
	'order'	    => 'DESC',
    'paged' => get_query_var( 'paged' ) ?: 1
];

$wpb_all_query = new WP_Query($args);
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section">
    <div class="container">
        <div class="events-list">
            <?php if ($wpb_all_query->have_posts()) : ?>
                 <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post();?>
                    <div class="event-item py-4">
                        <div class="image-wrapper">
                            <div class="image">
                                <div class="image-absolute">
                                    <?= getImage(get_post_thumbnail_id(), 'true') ?>
                                </div>
                            </div>
                        </div>
                        <div class="content-wrapper">
                            <h2 class="title-primary h3"><?= the_title() ?></h2>
                            <div class="h5-standard my-auto"><?= the_excerpt() ?></div>
                            <div class="button-wrapper text-center">
                                <a href="<?= get_permalink() ?>" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                            </div>
                        </div>
                    </div>
                    <hr class="hr-black">

                    <?php
                    $total_pages = $wpb_all_query->max_num_pages;
                    if ($total_pages > 1){
                        echo '<div class="pagination-box text-center my-4">';
                        $current_page = max(1, get_query_var('paged'));
                        echo paginate_links([
                            'current' => $current_page,
                            'total' => $total_pages,
                            'prev_text'    => '<',
                            'next_text'    => '>'
                        ]);
                        echo '</div>';
                    }

                    wp_reset_postdata(); ?>
                 <?php endwhile; ?>
            <?php else : ?>
                <p><?= __('Przepraszamy, brak wpisów spełniających podane kryteria.', 'siemczyno') ?></p>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php
get_footer();

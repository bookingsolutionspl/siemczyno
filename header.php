<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

$gtm = '';
$fields = get_fields();
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if (!empty($gtm)) { ?>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '<?= $gtm ?>');</script>
    <?php } ?>
    <meta name="theme-color" content="#ffffff"/>
    <?php wp_head(); ?>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Italianno&display=swap" rel="stylesheet">
    

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
</head>
<body>
<?php if (!empty($gtm)) { ?>
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
<?php } ?>

<header>
    <nav class="navbar navbar-expand-xxl">
        <div class="navbar-holder">
            <div class="logo">
                <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                    <div class="icon">
                        <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                    </div>
                </a>
            </div>
            <div class="navigation-wrapper">
                <?php
                wp_nav_menu(array(
                    'menu' => 'navigation',
                    'menu_class' => 'nav navbar-nav-alt',
                    'items_wrap' => '<ul class="navbar-nav-alt collapse navbar-collapse" id="navbarNav">%3$s</ul>',
                    'theme_location' => 'navigation',
                    'depth' => 2,
                    'walker' => new BootstrapNavMenuWalker()
                ));
                ?>

                <div class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Menu">
                    <div class="hamburger-menu"></div>
                </div>
            </div>

            <div class="social-lang-reserv-wrapper">
                <div class="social-wrapper">
                    <a rel="nofollow" href="<?= get_option('facebook') ?>" target="_blank">
                        <div class="icon">
                            <?= getImageSvgSrc("/dist/images/icons/socials/facebook.svg") ?>
                        </div>
                    </a>
                    <a rel="nofollow" href="<?= get_option('instagram') ?>" target="_blank">
                        <div class="icon">
                            <?= getImageSvgSrc("/dist/images/icons/socials/instagram.svg") ?>
                        </div>
                    </a>
                </div>

                <div class="lang-wrapper">

                <?php
                    foreach (pll_the_languages(['raw' => 1]) as $lang) { ?>
                        <a href="<?= $lang['url'] ?>">
                            <div class="icon">
                                <img src="<?= get_template_directory_uri(); ?>/dist/images/icons/flags/<?= $lang['slug'] . '.svg' ?>" alt="<?= $lang['name'] ?>">
                            </div>
                        </a>
                    <?php } ?>
                </div>

                <div class="button-wrapper">
                    <a href="<?= getTranslatedUrl('kontakt') ?>" class="btn btn-main"><?= __('Rezerwuj', 'siemczyno') ?></a>
                </div>
            </div>
        </div>
    </nav>
</header>

<?php if (is_front_page()) { ?>
    <div class="header-frontpage">
        <div id="splideHeader" class="splide">
            <div class="splide__track">
                <ul class="splide__list">
                    <?php foreach ($fields['slider'] as $slide) { if (!empty($slide)) { ?>
                        <div class="splide__slide">
                            <div class="image">
                                <?= getImage($slide, false) ?>
                            </div>
                        </div>
                    <?php }} ?>
                </ul>
            </div>
        </div>
        <div class="header-container text-center px-3">
            <h1 class="h0 title-primary amazone-regular text-white mb-2"><?= __('Pałac Siemczyno', 'siemczyno') ?></h1>
            <br>
            <h2 class="h3-standard subtitle-primary text-white"><?= __('Miejsce, w którym historia łączy się z teraźniejszością', 'siemczyno') ?></h2>
        </div>
    </div>

    
    <?php } ?>
<main>
<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header();
?>

<div class="header-container">
    <div class="header-plain-background"></div>
</div>

<div class="section big">
    <p class="h0 text-center font-weight-bold">404</p>
    <div class="text-center">
        <p class="caption-404 mb-5"><?= __('Ta strona nie istnieje.', 'siemczyno'); ?></p>
        <a href="<?= home_url() ?>" class="btn btn-main hidden-content" content="<?= __('Wróć na stronę główną', 'siemczyno'); ?>"></a>
    </div>
</div>

<?php
get_footer();

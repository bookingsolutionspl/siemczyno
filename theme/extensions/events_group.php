<?php

if (!function_exists('eventGroupTaxonomy')) {
    function eventGroupTaxonomy()
    {
        register_taxonomy('event_group_type', 'events_group', [
            'labels' => [
                'name' => 'Kategorie',
                'singular_name' => 'Kategoria',
            ],
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_columns' => true,
            'rewrite' => ['slug' => 'a'],
            'hierarchical' => true,
        ]);
    }
}

add_action('init', 'eventGroupTaxonomy', 0);

if (!function_exists('eventGroupPost')) {
    function eventGroupPost()
    {
        register_post_type('events_group', [
            'label' => 'events_group',
            'rewrite' => array(
                'slug' => 'wydarzenia_grupa'
            ),
            'labels' => [
                'name' => 'Wydarzenia',
                'singular_name' => 'Wydarzenie_grupa',
            ],
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'excerpt',
            ],
            'query_var' => true,
            'taxonomies' => ['event_group_type'],
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-list-view',
            'can_export' => true,
            'has_archive' => 'events_group',
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        ]);
    }
}

add_action('init', 'eventGroupPost', 0);
<?php

if (!function_exists('offerTaxonomy')) {
    function offerTaxonomy()
    {
        register_taxonomy('offer_type', 'offers', [
            'labels' => [
                'name' => 'Kategorie',
                'singular_name' => 'Kategoria',
            ],
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_columns' => true,
            'rewrite' => ['slug' => 'a'],
            'hierarchical' => true,
        ]);
    }
}

add_action('init', 'offerTaxonomy', 0);

if (!function_exists('offerPost')) {
    function offerPost()
    {
        register_post_type('offers', [
            'label' => 'offers',
            'rewrite' => array(
                'slug' => 'oferty'
            ),
            'labels' => [
                'name' => 'Oferty',
                'singular_name' => 'Oferta',
            ],
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'excerpt',
            ],
            'query_var' => true,
            'taxonomies' => ['offer_type'],
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-list-view',
            'can_export' => true,
            'has_archive' => 'offers',
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        ]);
    }
}

add_action('init', 'offerPost', 0);
<?php

if (!function_exists('attractionGroupTaxonomy')) {
    function attractionGroupTaxonomy()
    {
        register_taxonomy('attraction_group_type', 'attractions_group', [
            'labels' => [
                'name' => 'Kategorie',
                'singular_name' => 'Kategoria',
            ],
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_columns' => true,
            'rewrite' => ['slug' => 'a'],
            'hierarchical' => true,
        ]);
    }
}

add_action('init', 'attractionGroupTaxonomy', 0);

if (!function_exists('attractionGroupPost')) {
    function attractionGroupPost()
    {
        register_post_type('attractions_group', [
            'label' => 'attractions_group',
            'rewrite' => array(
                'slug' => 'atrakcje_grupa'
            ),
            'labels' => [
                'name' => 'Atrakcje',
                'singular_name' => 'Atrakcja_grupa',
            ],
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'excerpt',
            ],
            'query_var' => true,
            'taxonomies' => ['attraction_group_type'],
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-list-view',
            'can_export' => true,
            'has_archive' => 'attractions_group',
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        ]);
    }
}

add_action('init', 'attractionGroupPost', 0);
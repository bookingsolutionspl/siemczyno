<?php

/**
 * Return image from source
 *
 * @param string $src
 * @param string $alt
 * @param bool $lazy
 * @param string $fancy
 * @param string $class
 *
 * @return string
 */
if (!function_exists('getImageSvgSrc')) {
    function getImageSvgSrc($src, $class = '')
    {
        try {
            $dom = new DOMDocument();
            libxml_use_internal_errors(true);
            // var_dump(ABSPATH .'wp-content/themes/siemczyno'. $src);
            $dom->loadHTMLFile(ABSPATH .'wp-content/themes/siemczyno'. $src);
            foreach ($dom->getElementsByTagName('svg') as $element) {
                $element->setAttribute('class', $class);
            }
            foreach ($dom->getElementsByTagName('path') as $element) {
                $element->removeAttribute('fill');
            }
            foreach ($dom->getElementsByTagName('rect') as $element) {
                $element->removeAttribute('fill');
            }

            $dom->saveHTML();
            libxml_clear_errors();
            $svg = preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $dom->saveHTML());

            return $svg;
        } catch (Exception $e) {
            return "Can't find {$src}";
        }
    }
}
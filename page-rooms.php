<?php
/**
 * Template Name: Pokoje
 */

get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="bg-image overflow-hidden">
    <div class="image">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
    <div class="section section-p-big pb-0">
        <div class="container">
            <div class="narrow">
                <div class="logo-home-page mb-5 entry">
                    <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                </div>
                <div class="mb-5 entry">
                    <div class="content text-center narrow h5-standard mb-5 entry">
                        <?= $fields['opis'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section pb-0">
    <div class="container">
        <div class="title-wrapper text-center mb-6 entry">
            <h2 class="title-primary underline h2"><?= $fields['tytul'] ?></h2>
        </div>
        <div class="rooms-box-main rooms-box-main-infinite">
            <div class="row-1">
                <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type' => 'post', 'category_name' => 'pokoje', 'posts_per_page' => -1, 'order' => 'asc')) ?>

                <?php if ($wpb_all_query->have_posts()) : ?>
                    <!-- the loop -->

                    <?php
                    while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); $fieldsPost = get_fields($post->ID);?>


                            <div class="item item-secondary entry">
                                <div class="image-wrapper">
                                    <div class="image">
                                        <?= getImage(get_post_thumbnail_id(), 'true') ?>
                                    </div>
                                </div>
                                <div class="content-wrapper">
                                    <div class="content">
                                        <h2 class="title-primary text-white h3 mb-auto"><?= the_title() ?></h2>
                                        <h3 class="title-primary text-white h4 price-right"><?= $fieldsPost['charakterystyka']['ceny']['od'] ?></h3>
                                        
                                        <div class="button-wrapper">
                                            <hr class="mb-4 hr-white">
                                            <a href="<?= get_permalink() ?>" class="btn btn-third third-white"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php
                    endwhile; ?>

                    <?php wp_reset_postdata(); ?>

                <?php else : ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<div class="section entry">
    <div class="container">
        <div class="narrow narrow-sm">
                <?php get_template_part('template-parts/form', 'contact'); ?>
        </div>
    </div>
</div>


<?php
get_footer();
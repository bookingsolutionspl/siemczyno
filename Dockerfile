FROM wordpress:latest

COPY ./docker-entrypoint.sh /var/www/docker-entrypoint.sh
RUN ["chmod", "+x", "/var/www/docker-entrypoint.sh"]

<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Business data
 */
add_action('admin_menu', 'add_gcf_interface');

function add_gcf_interface() {
	add_options_page('Zmienne globalne', 'Zmienne globalne', '8', 'functions', 'editglobalcustomfields');
}

function editglobalcustomfields() {
	?>
	<div class='wrap'>
	<h2>Zmienne globalne</h2>
	<form method="post" action="options.php">
	<?php wp_nonce_field('update-options') ?>

	<p><strong>Nazwa obiektu:</strong><br />
	<input type="text" name="name" size="45" value="<?php echo get_option('name'); ?>" /></p>
	
	<p><strong>Email:</strong><br />
	<input type="text" name="email_1" size="45" value="<?php echo get_option('email_1'); ?>" /></p>

	<p><strong>Adres - ulica</strong><br />
	<input type="text" name="address_1" size="45" value="<?php echo get_option('address_1'); ?>" /></p>

    <p><strong>Adres - miasto</strong><br />
	<input type="text" name="address_2" size="45" value="<?php echo get_option('address_2'); ?>" /></p>

    <p><strong>Telefon - 1</strong><br />
    <input type="text" name="phone_1" size="45" value="<?php echo get_option('phone_1'); ?>" /></p>

    <p><strong>Telefon - 2</strong><br />
    <input type="text" name="phone_2" size="45" value="<?php echo get_option('phone_2'); ?>" /></p>

    <p><strong>Telefon - stacjonarny</strong><br />
    <input type="text" name="phone_3" size="45" value="<?php echo get_option('phone_3'); ?>" /></p>

    <p><strong>NIP</strong><br />
    <input type="text" name="nip" size="45" value="<?php echo get_option('nip'); ?>" /></p>

    <p><strong>Facebook</strong><br />
    <input type="text" name="facebook" size="45" value="<?php echo get_option('facebook'); ?>" /></p>

    <p><strong>Instagram</strong><br />
    <input type="text" name="instagram" size="45" value="<?php echo get_option('instagram'); ?>" /></p>
    
    <p><strong>Mapa</strong><br />
	<input type="text" name="map" size="45" value="<?php echo get_option('map'); ?>" /></p>

    <p><strong>Numer konta</strong><br />
    <input type="text" name="account_number" size="45" value="<?php echo get_option('account_number'); ?>" /></p>
    
    <p><input type="submit" name="Submit" value="Zaktualizuj" /></p>
    
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="page_options" value="name,email_1,address_1,address_2,phone_1,phone_2,phone_3,nip,facebook,instagram,map,account_number" />

	</form>
	</div>
	<?php
}

/**
 * Adding defer to scripts
 */
function gioga_add_defer_attribute($tag, $handle) {
	if ( 'script' !== $handle )
	return $tag;
	return str_replace( ' src', ' defer src', $tag );
}
add_filter('script_loader_tag', 'gioga_add_defer_attribute', 10, 2);

/**
 * Enqueue versioned styles
 */
function siemczyno_theme_styles()  
{
  global $ver_num; // define global variable for the version number
  $ver_num = mt_rand(); // on each call/load of the style the $ver_num will get different value
  wp_enqueue_script( 'script', get_template_directory_uri() . '/dist/main.bundle.js', array(), $ver_num, true);
  wp_enqueue_style( 'style-css', get_template_directory_uri() . '/dist/styles.css', array(), $ver_num, 'all' );
}
add_action('wp_enqueue_scripts', 'siemczyno_theme_styles');

/**
 * Exclude filter files when exporting in All In One Migration
 */
add_filter('ai1wm_exclude_content_from_export', function($exclude_filters) {
    $path = 'themes/siemczyno/';
    $exclude_filters[] = $path .'node_modules';
    $exclude_filters[] = $path .'.git';
    $exclude_filters[] = $path .'.idea';
    $exclude_filters[] = $path .'.vscode';
    $exclude_filters[] = $path .'package-lock.json';
    return $exclude_filters;
});

/**
 * Remove JQuery
 */
function removeJquery()
{
    wp_deregister_script('jquery');
}

add_action('wp_enqueue_scripts', 'removeJquery');

/**
 * Remove unnecessary Wordpress features
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

/**
 * Remove JSON API links in header
 */
function remove_json_api()
{
    remove_action('wp_head', 'rest_output_link_wp_head', 10);
    remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
    remove_action('rest_api_init', 'wp_oembed_register_route');
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('wp_head', 'wp_oembed_add_host_js');
    add_filter('embed_oembed_discover', '__return_false');
}

add_action('after_setup_theme', 'remove_json_api');

/**
 * Disable the REST API
 */
function disable_json_api()
{
    add_filter('json_enabled', '__return_false');
    add_filter('json_jsonp_enabled', '__return_false');
    add_filter('rest_enabled', '__return_false');
    add_filter('rest_jsonp_enabled', '__return_false');
}

add_action('after_setup_theme', 'disable_json_api');

function wps_deregister_styles()
{
    wp_dequeue_style('wp-block-library');
}

add_action('wp_print_styles', 'wps_deregister_styles', 100);

/**
 * Return image from wordpress gallery
 *
 * @param $id
 * @param bool $lazy
 * @param string $fancy
 * @param string $size
 * @param string $class
 * @return string
 */
function getImage($id, $lazy = false, $gLightbox = '', $size = 'full', $class = '')
{
    $wanted = wp_get_attachment_image_srcset($id, $size);

    $fullImage = wp_get_attachment_image_src($id, 'large')[0];

    $alt = get_post_meta($id, '_wp_attachment_image_alt')[0];

    $size = $wanted ? $wanted : $fullImage;

    $gLightbox = !empty($gLightbox) ? 'class=' . $gLightbox : '';

    $result = $gLightbox ? '<a ' . $gLightbox . ' href="' . $fullImage . '">' : '';
    $result .= !$lazy ? '<img srcset="' . $size . '" class="' . $class . '"' : '';
    $result .= $lazy ? '<img srcset="' . get_template_directory_uri() . '/dist/images/image-placeholder.png" data-srcset="' . $size . '" class="lazy ' . $class . '"' : '';
    $result .= $lazy ? ' alt="placeholder-image"' : $alt ? ' alt="' . $alt . '"' : '';
    $result .= $alt && $lazy ? ' data-alt="' . $alt . '"' : '';
    $result .= !$lazy && !empty($class) ? ' class="' . $class . '"' : '';
    $result .= '>';
    $result .= $lazy ? '<noscript><img src="' . $size . '" alt="' . $alt . '"></noscript>' : '';
    $result .= $gLightbox ? '</a>' : '';

    return $result;
}

/**
 * Return translated URL
 *
 * @param $slug
 * @return string
 */
function getTranslatedUrl($slug)
{
    return get_the_permalink(pll_get_post(get_page_by_path($slug)->ID));
}

/**
 * Return translated Post URL
 *
 * @param $slug
 * @return string
 */
function getTranslatedPostUrl($slug)
{   
    $post = get_page_by_path($slug, OBJECT, 'post');
    return get_the_permalink(pll_get_post($post->ID));
}

if (!function_exists('setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function setup()
    {
        /**
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');
        
        /*
         * Make theme available for translation.
         */
        load_theme_textdomain('siemczyno', get_template_directory() . '/translations');

        /**
         * Register theme navigations.
         */
        register_nav_menus(array(
            'navigation' => esc_html__('navigation', 'siemczyno'),
            'navigation-footer' => esc_html__('navigation-footer', 'siemczyno'),
        ));
    }
endif;

add_action('after_setup_theme', 'setup');

/**
 * Extended Walker class for use with the
 * Twitter Bootstrap toolkit Dropdown menus in Wordpress.
 * Edited to support n-levels submenu.
 * @author johnmegahan https://gist.github.com/1597994, Emanuele 'Tex' Tessore https://gist.github.com/3765640
 */
class BootstrapNavMenuWalker extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = [])
    {
        $indent = str_repeat("\t", $depth);
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $li_attributes = '';
        $value = '';
        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $divider_class_position = array_search('divider', $classes);

        if ($divider_class_position !== false) {
            $output .= "<li class=\"divider\"></li>\n";
            unset($classes[$divider_class_position]);
        }

        $classes[] = ($args->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
        $classes[] = 'menu-item-' . $item->ID;
        if ($depth && $args->has_children) {
            $classes[] = 'dropdown-submenu';
        }

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = ' class="' . esc_attr($class_names) . ' nav-item"';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';

        if ($args->has_children) {
            $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        } else {
            $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        }

        $attributes .= ($args->has_children) ? ' data-toggle="dropdown"' : '';

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= ($depth == 0 && $args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }


    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        if (!$element)
            return;

        $id_field = $this->db_fields['id'];

        if (is_array($args[0]))
            $args[0]['has_children'] = !empty($children_elements[$element->$id_field]);
        else if (is_object($args[0]))
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'start_el'), $cb_args);

        $id = $element->$id_field;

        if (($max_depth == 0 || $max_depth > $depth + 1) && isset($children_elements[$id])) {
            foreach ($children_elements[$id] as $child) {
                if (!isset($newlevel)) {
                    $newlevel = true;

                    $cb_args = array_merge(array(&$output, $depth), $args);
                    call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
                }
                $this->display_element($child, $children_elements, $max_depth, $depth + 1, $args, $output);
            }
            unset($children_elements[$id]);
        }

        if (isset($newlevel) && $newlevel) {
            $cb_args = array_merge(array(&$output, $depth), $args);
            call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
        }

        $cb_args = array_merge(array(&$output, $element, $depth), $args);
        call_user_func_array(array(&$this, 'end_el'), $cb_args);
    }
}
require 'theme/extensions/images.php';
require 'theme/extensions/attractions_group.php';
require 'theme/extensions/offers.php';
require 'theme/extensions/events_group.php';
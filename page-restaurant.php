<?php
/**
 * Template Name: Restauracja
 */


get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="bg-image overflow-hidden">
    <div class="image">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
    <div class="section section-p-big pb-0">
        <div class="container">
            <div class="narrow">
                <div class="logo-home-page mb-5 entry">
                    <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                </div>
                <div class="title-wrapper text-center mb-5 entry">
                    <h2 class="title-primary line-decoration h2"><?= $fields['sekcja_1']['tytul'] ?></h2>
                </div>
                <div class="mb-5 entry">
                    <div class="content text-center narrow h5-standard mb-5 entry">
                        <?= $fields['sekcja_1']['opis'] ?>
                    </div>
                    <div class="button-wrapper text-center scrollFrom">
                        <button type="button" class="btn btn-main">
                            <?= __('Czytaj więcej', 'siemczyno') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="narrow">
        <div class="three-images">
            <div class="images-wrapper">
                <div class="three-image entry">
                    <?= getImage($fields['sekcja_1']['zdjecie_1'], true) ?>
                </div>
                <div class="three-image entry">
                    <?= getImage($fields['sekcja_1']['zdjecie_2'], true) ?>
                </div>
                <div class="three-image entry">
                    <?= getImage($fields['sekcja_1']['zdjecie_3'], true) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section scrollTo pb-0">
    <div class="container">
        <div class="download-banner entry">
            <div class="logo">
                <div class="icon">
                    <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                </div>
            </div>
            <div class="content h6">
                <p class="text-uppercase text-white font-weight-bold mb-2"><?= $fields['sekcja_czarna']['opis_1'] ?></p>
                <p class="text-white h4 mb-0"><?= $fields['sekcja_czarna']['opis_2'] ?></p>
            </div>
            <div class="link-wrapper">
                <div class="link">
                    <a href="<?= $fields['sekcja_czarna']['pdf'] ?>" target="_blank" class="download-link h7 mb-0">
                        <div class="icon">
                            <?= getImageSvgSrc("/dist/images/icons/others/pdf.svg") ?>
                        </div>
                        <p class="mb-0"><?= $fields['sekcja_czarna']['opis_3'] ?></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section bg-pattern overflow-hidden pb-0">
    <div class="container">
        <div class="narrow scrollTo">
            <div class="main-content-box">
                <div class="first-half entry entry-left">
                    <div class="image-wrapper">
                        <div class="image">
                            <?= getImage($fields['sekcja_2']['zdjecie'], true) ?>
                        </div>
                    </div>
                </div>
                <div class="second-half">
                    <div class="content text-center text-xl-start">
                        <h2 class="h3 title-primary"><?= $fields['sekcja_2']['tytul'] ?></h2>
                        <hr class="my-4">
                        <div class="text h5-standard mb-5">
                            <?= $fields['sekcja_2']['opis'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section-gallery">
    <div class="container">
        <div class="title-wrapper text-center mb-5 entry">
            <h2 class="title-primary h3 mb-2"><?= __('Galeria', 'siemczyno') ?></h2>
            <hr class="my-4">
        </div>
        <div class="gallery standard">
            <?php
                if ($fields['galeria']) {
                    foreach ($fields['galeria'] as $key => $img) { 
                        if(!empty($img)) {
                            if (strpos($key, 'zdjecie_') !== false) {
                                echo '<div class="image entry">' .
                                getImage($img, 'true', 'glightbox')
                                . '</div>';
                            }
                        }
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php
get_footer();

<?php
/**
 * Template Name: Oferta
 * 
 * Template Post Type: post
 */

get_header();

$fields = get_fields();
$currentPostId = get_the_ID();
?>

<?php get_template_part('template-parts/header', 'secondary');

while ( have_posts() ) : the_post();
$currentPostId = get_the_ID();
?>

<div class="section pb-0">
    <div class="container">
        <div class="title-ornament mb-5">
            <h2 class="title-primary h3"><?= $fields['informacje']['tytul'] ?></h2>
            <div class="separator"></div>
        </div>
        <div class="additional-info h6 p-ornament">
            <?= $fields['informacje']['opis_1'] ?>
            <p class="mt-5"><?= __('Telefon', 'siemczyno') ?>: <a href="tel:<?= get_option('phone_2') ?>"><?= get_option('phone_2') ?></a></p>
            <p><?= __('Telefon / fax', 'siemczyno') ?>: <a href="tel:<?= get_option('phone_3') ?>"><?= get_option('phone_3') ?></a></p>
            <p><?= __('Email', 'siemczyno') ?>: <a href="mailto:<?= get_option('email_1') ?>"><?= get_option('email_1') ?></a></p>
            <?= $fields['informacje']['opis_2'] ?>
        </div>
        <div class="buttons-wrapper text-center text-md-start p-ornament d-flex flex-column flex-md-row justify-content-center justify-content-md-start mt-5">
            <button class="btn btn-main mb-3 mb-md-0 me-0 me-md-3 call-bse-widget scrollFromContact"><?= __('Rezerwuj', 'siemczyno') ?></button>
            <button type="button" class="btn btn-second scrollFrom"><?= __('Galeria', 'siemczyno') ?></button>
        </div>
    </div>
</div>



<div class="section pb-0">
    <div class="container">
        <div class="title-ornament mb-3">
            <h2 class="title-primary h3"><?= $fields['udogodnienia']['tytul'] ?></h2>
            <div class="separator"></div>
        </div>
        <div class="list-dashed p-ornament">
            <?php foreach ($fields['udogodnienia'] as $udogodnienie) { if(!empty($udogodnienie['nazwa'])) {?>
                <div class="item">
                    <p class="h6">
                        <?= getImageSvgSrc("/dist/images/icons/features/". $udogodnienie['ikona']) ?>
                        <?= $udogodnienie['nazwa'] ?>
                    </p>
                </div>
            <?php }} ?>
        </div>
    </div>
</div>



<div class="section section-gallery scrollTo">
    <div class="container">
        <div class="title-wrapper text-center mb-5">
            <h2 class="title-primary h3 mb-2"><?= $fields['galeria']['tytul'] ?></h2>
            <hr class="my-4">
        </div>         
        <div class="gallery standard">
            <?php
                if ($fields['galeria']) {
                    foreach ($fields['galeria'] as $key => $img) { 
                        if(!empty($img)) {
                            if (strpos($key, 'zdjecie_') !== false) {
                                echo '<div class="image entry">' .
                                getImage($img, 'true', 'glightbox', 'large')
                                . '</div>';
                            }
                        }
                    }
                }
            ?>
        </div>
    </div>
</div>

<div class="section bg-image pt-0 scrollToContact">
    <div class="image top-0">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
        <div class="container">
            <div class="narrow narrow-sm">
                    <?php get_template_part('template-parts/form', 'contact'); ?>
            </div>
        </div>
    </div>
</div>


<?php endwhile; ?>

<?php
get_footer();
<?php

/**
 * Template Name: Atrakcje
 */

get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section">
    <div class="container">
        <div class="list-with-sidebar-wrapper">

            <?php 
            $taxQuery = [];

            if (isset($_GET['category'])) {
                array_push($taxQuery, [
                    'taxonomy' => 'attraction_group_type',
                    'field' => 'slug',
                    'terms' => $_GET['category'] ?: '',
                ]);
            }

            $args = [
                'post_type' => 'attractions_group',
                'tax_query' => $taxQuery,
                'posts_per_page' => 5,
                'order' => 'desc',
                'paged' => get_query_var( 'paged' ) ?: 1
            ];

            $wpb_all_query = new WP_Query($args)
            ?>

            <?php if ($wpb_all_query->have_posts()) : ?>

                <div class="sidebar-wrapper pt-4">
                    <div class="sidebar filters">
                        
                        <a href="<?= getTranslatedUrl('atrakcje') ?>" class="item-wrapper">
                            <div class="item" data-category="without-category">
                                <div class="icon-wrapper">
                                </div>
                                <div class="title-wrapper">
                                    <h6 class="h5-standard">
                                        <?= __('Wszystkie', 'siemczyno'); ?>
                                    </h6>
                                </div>
                            </div>
                        </a>

                            <?php foreach (get_categories(['taxonomy' => 'attraction_group_type', 'orderby' => 'term_id', 'order' => 'asc']) as $key => $category) {
                                $selected = $_GET['category'] ? 'selected' : '';
                                ?>
                                <a href="<?= getTranslatedUrl('atrakcje') . '?category=' .$category->slug ?>" class="item-wrapper">
                                    <div class="item" data-category="<?= $category->slug ?>">
                                        <div class="icon-wrapper">
                                        </div>
                                        <div class="title-wrapper">
                                            <h6 class="h5-standard">
                                                <?= $category->name ?>
                                            </h6>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>

                        </div>
                    </div>

                    <div class="list-wrapper">
                        <!-- the loop -->
                        <?php
                        while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); $fieldsPost = get_fields($post->ID);?>
                            <div class="item-wrapper py-4">
                                <div class="image-wrapper">
                                    <div class="image">
                                        <?= getImage(get_post_thumbnail_id(), 'true') ?>
                                    </div>
                                </div>
                                <div class="content-wrapper">
                                    <h2 class="title-primary h3"><?= the_title() ?></h2>
                                    <div class="h5-standard my-auto"><?= the_excerpt() ?></div>
                                    <div class="button-wrapper text-center">

                                    <?php if(!empty($fieldsPost['link'] )) { ?>

                                        <?php if(in_array($post->ID, array(1411, 1486, 1139))) { ?>
                                            <a href="<?= $fieldsPost['link'] ?>" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                        <?php } else { ?>
                                            <a href="<?= $fieldsPost['link'] ?>" target="_blank" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                        <?php } ?>
                                        
                                    <?php } else { ?>
                                        <a href="<?= get_permalink() ?>" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <hr class="hr-black">
                        <?php
                        endwhile; ?>
                    </div>
                    

                    <?php
                    $total_pages = $wpb_all_query->max_num_pages;
                    if ($total_pages > 1){
                        echo '<div class="pagination-box text-center my-4">';
                        $current_page = max(1, get_query_var('paged'));
                        echo paginate_links([
                            'current' => $current_page,
                            'total' => $total_pages,
                            'prev_text'    => '<',
                            'next_text'    => '>'
                        ]);
                        echo '</div>';
                    }

                    wp_reset_postdata(); ?>

                <?php else : ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>

                </div>
        </div>
    </div>

    <script>
        const url = window.location.href;
        const categoryIndex = url.indexOf('?');

        if(categoryIndex > 0) {
            const categoryValue = url.substring(categoryIndex + 10);

            element = document.querySelectorAll("[data-category=" + categoryValue +"]");
            element[0].classList.add("active");
            console.log(element[0]);
        } else {
            element = document.querySelectorAll("[data-category=without-category]");
            element[0].classList.add("active");
            console.log(element[0]);
        }
    </script>

    <?php
    get_footer();

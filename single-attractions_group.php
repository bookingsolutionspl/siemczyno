<?php
/**
 * Template Name: Atrakcja
 * 
 * Template Post Type: post
 */

get_header();

$fields = get_fields();
$currentPostId = get_the_ID();
?>

<?php get_template_part('template-parts/header', 'secondary');

while ( have_posts() ) : the_post();
$currentPostId = get_the_ID();
?>

<div class="bg-image overflow-hidden">
    <div class="image">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
    <div class="section section-p-big">
        <div class="container">
            <div class="narrow">
                <div class="logo-home-page mb-5 entry">
                    <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                </div>
                <div class="mb-5 entry">
                    <div class="content text-center narrow h6 mb-5 entry">
                        <?= $fields['opis'] ?>
                    </div>
                    <div class="button-wrapper text-center">
                        <?php if(!empty($fields['link_wewnetrzny']['url'])) { ?>
                            <a href="<?= $fields['link_wewnetrzny']['url'] ?>" target="_blank" class="btn btn-main"><?=  __('Czytaj więcej', 'siemczyno') ?></a>
                        <?php } else if(!empty($fields['pdf'])) { ?>
                            <a href="<?= $fields['pdf'] ?>" target="_blank" class="btn btn-main"><?=  __('Czytaj więcej', 'siemczyno') ?></a>
                        <?php } ?> 
                    </div>
                </div>
            </div>

            <?php if (!empty($fields['galeria']['obraz_1'])) { ?>
                <div class="title-wrapper text-center scrollTo">
                    <h2 class="title-primary h3 mb-2"><?= __('Galeria', 'siemczyno') ?></h2>
                    <hr class="my-4">
                </div>         
                <div class="gallery standard">
                    <?php
                        if ($fields['galeria']) {
                            foreach ($fields['galeria'] as $img) { if(!empty($img)) {
                                echo '<div class="image entry no-background-image">' .
                                getImage($img, 'true', 'glightbox', 'large')
                                . '</div>';
                            }}
                        }
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php endwhile; ?>

<?php
get_footer();
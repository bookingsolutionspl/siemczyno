<div class="coupon-wrapper">
    <div class="icon" id="coupon">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/icons/coupon/shield.svg" alt="covid info">
    </div>
    <div class="cancel d-none">
        <div class="icon">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/icons/coupon/cancel.svg" alt="cancel">
        </div>
    </div>
    <div class="info-box d-none">
        <div class="icon">
            <img src="<?= get_template_directory_uri(); ?>/dist/images/icons/coupon/bon-turystyczny.png" alt="bon turystyczny">
        </div>
        <div class="text mb-2"><?= __('U nas zrealizujesz polski bon turystyczny', 'siemczyno') ?></div>
    </div>
</div>
<div class="media-widget foodorder-widget">
    <a href="https://www.czaplinek360.pl/palacsiemczyno/" target="_blank" class="item text-decoration-none">
        <div class="icon">
            <?= getImageSvgSrc("/dist/images/icons/others/eye.svg") ?>
        </div>
        <div class="content">
            <span class="text h8 mb-0"><?= __('Wirtualny spacer', 'siemczyno') ?></span>
        </div>
    </a>
</div>
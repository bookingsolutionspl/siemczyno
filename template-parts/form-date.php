<form class="form-date" method="post" action="<?= getTranslatedUrl('pokoje') ?>#contact-room">

    <div class="date-box">
        <div class="title"><?= __('Kiedy?', 'siemczyno') ?></div>
    
        <div class="input-container">
            <div class="input-wrapper">
                <input class="date" name="date_range" placeholder="<?= __('Wybierz datę..', 'siemczyno') ?>" type="text" id="dateRange" required/>
            </div>
        </div>
    
        <div class="button-wrapper">
            <button type="submit" name="send" class="btn btn-main btn-transparent">
                <div class="box">
                    <div class="text">
                        <?= __('Rezerwuj', 'siemczyno') ?>
                    </div>
                </div>
            </button>
        </div>
    </div>
</form>
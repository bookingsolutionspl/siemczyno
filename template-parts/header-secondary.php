<?php global $post; if (!is_home() && has_post_thumbnail()) { $content = get_field('under_content'); ?>
    <div class="header-container">
        <div class="header-image-background">
            <div class="image-cover" style="background-image: url(<?= the_post_thumbnail_url('original') ?>)"></div>
            <div class="content text-center">
                <h1 class="h1 subtitle-primary italianno text-white mb-0"><?php the_title() ?></h1>
            </div>
        </div>
    </div>
<?php } ?>

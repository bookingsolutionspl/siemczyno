<div class="form-wrapper">
    <div class="title-wrapper text-center mb-5">
        <h2 class="title-primary underline h3 mb-3"><?= __('Formularz Kontaktowy', 'siemczyno') ?></h2>
        <p class="h5-standard text-center"><?= __('Skontaktuj się z nami', 'siemczyno') ?>!</p>
    </div>
    <form id="contact-form" class="row g-3 h6" novalidate>
        <div class="form-group validate mb-2" data-validate="required|person">
            <label for="name" class="form-label">
                <div class="icon">
                    <?= getImageSvgSrc("/dist/images/icons/form/user.svg") ?>
                </div>
            </label>
            <input type="text" class="form-control" id="name" name="name" placeholder="<?= __('imię i nazwisko', 'siemczyno') ?>" required>
        </div>
        <div class="form-group validate mb-2" data-validate="required|email">
            <label for="email" class="form-label">
                <div class="icon">
                    <?= getImageSvgSrc("/dist/images/icons/form/email.svg") ?>
                </div>
            </label>
            <input type="email" class="form-control" id="email" name="email" placeholder="<?= __('adres email', 'siemczyno') ?>">
        </div>
        <div class="form-group validate mb-2" data-validate="required">
            <label for="phone" class="form-label">
                <div class="icon">
                    <?= getImageSvgSrc("/dist/images/icons/form/call.svg") ?>
                </div>
            </label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="<?= __('telefon', 'siemczyno') ?>">
        </div>
        <div class="form-group mb-2">
            <label for="dateRange" class="form-label">
                <div class="icon">
                    <?= getImageSvgSrc("/dist/images/icons/form/calendar.svg") ?>
                </div>
            </label>
            <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="<?= __('data pobytu', 'siemczyno') ?>">
        </div>
        <div class=" form-group validate mb-2" data-validate="required">
            <label for="message" class="form-label">
                <div class="icon">
                    <?= getImageSvgSrc("/dist/images/icons/form/file.svg") ?>
                </div>
                </label>
            <textarea class="form-control" id="message" name="message" rows="3" placeholder=<?= __('wiadomość', 'siemczyno') ?>></textarea>
        </div>
        <div class="form-check validate mb-4" data-validate="checkRequired">
            <div class="input-wrapper">
                <input class="form-check-input required-check" type="checkbox" value="" id="agree" name="agree" required="">
            </div>
            <label class="form-check-label h7" for="agree">
                <?= __('Wyrażam zgodę na przetwarzanie podanych danych osobowych oraz na kontakt ze strony Pałacu Siemczyno', 'siemczyno') ?>
            </label>
        </div>
        <div class="spinner-border mx-4 mb-4 d-none" role="status"></div>
        <div class="button-wrapper">
            <button class="btn btn-main" type="submit"><?= __('Wyślij', 'siemczyno') ?></button>
        </div>
        <div class="message-success d-none mt-4">
            <p class="mb-0"><?= __('Twoja wiadomość została wysłana. Dziękujemy.', 'siemczyno') ?></p>
        </div>
        <div class="message-fail d-none mt-4">
            <p class="mb-0"><?= __('Twoja wiadomość nie została dostarczona. Spróbuj ponownie.', 'siemczyno') ?></p>
        </div>
    </form>
</div>
<?php

$args = [
    'post_type' => 'events_group',
    'meta_query'	=> array(
        array(
            'key' => 'data',
            'value' => date('Y-m-d H:i:s'),
            'compare' => '>='
            )),
    'meta_key'  => 'data',
	'orderby'   => 'meta_value',
	'order'	    => 'ASC'
];

$wpb_all_query = new WP_Query($args);
?>



<?php if ($wpb_all_query->have_posts()) : ?>
    <div class="splide-event-wrapper">
        <div id="splideEvents" class="splide">
            <div class="splide__track">
                <ul class="splide__list">

                    <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post();  $fieldsPost = get_fields($post->ID);?>
                        <div class="splide__slide">
                            <div class="splide-event-container">
                                <a href="<?= get_permalink() ?>">
                                    <div class="image-wrapper">
                                        <div class="image">
                                            <?= getImage(get_post_thumbnail_id(), false) ?>
                                        </div>
                                    </div>
                                    <div class="content-box">
                                        <h2 class="h7 cut-text"><?= the_title() ?></h2>
                                        <div class="date-box">
                                            <h2 class="h4-standard text-center day"><?= date_i18n('d', strtotime($fieldsPost['data'])) ?></h2>
                                            <h2 class="h7 text-center"><?= date_i18n('M', strtotime($fieldsPost['data'])) ?></h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php $i++; endwhile;?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>
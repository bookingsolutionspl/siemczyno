<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section bg-white">
    <div class="container h6">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php get_template_part('template-parts/title', 'primary'); ?>
            <?php if (!empty($fields['tresc_glowna'])) {?>
                <?= $fields['tresc_glowna'] ?>
            <?php } ?>
            
            <?php the_content(); ?>

        </article>
    </div>
</div>

<?php
get_footer();

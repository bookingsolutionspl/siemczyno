<?php
/**
 * Template Name: Polityka prywatności
 */


get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'secondary'); ?>

<div class="section bg-white">
    <div class="container h6">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php get_template_part('template-parts/title', 'primary'); ?>
            <?php if (!empty($fields['tresc_glowna'])) {?>
                <?= $fields['tresc_glowna'] ?>
            <?php } ?>
            
            <?php the_content(); ?>

        </article>
    </div>
</div>

<?php
get_footer();

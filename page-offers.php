<?php
/**
 * Template Name: Oferta
 */


get_header();
$fields = get_fields();

$taxQuery = [];

if (isset($_GET['category'])) {
    array_push($taxQuery, [
        'taxonomy' => 'offer_type',
        'field' => 'slug',
        'terms' => $_GET['category'] ?: '',
    ]);
}

$args = [
    'post_type' => 'offers',
    'tax_query' => $taxQuery,
];

$i = 1;

$wpb_all_query = new WP_Query($args)
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section-wrapper">
    <?php if ($wpb_all_query->have_posts()) : ?>
        <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); 
        $modulo = $i % 2; ?>
            <div class="section">
                <div class="container entry">
                    <div class="<?= $modulo ? 'offer-box ' : 'offer-box-revert' ?>">
                        <div class="content-box">
                            <div class="image-wrapper ">
                                <div class="image">
                                    <?= getImage(get_post_thumbnail_id(), 'true') ?>
                                </div>
                            </div>
                            <div class="content-wrapper">
                                <h2 class="h3 title-primary"><?= the_title() ?></h2>
                                <hr>
                                <div class="text h5-standard my-auto">
                                    <?= the_excerpt() ?>
                                </div>
                                <div class="button-wrapper text-center">
                                    <a href="<?= get_permalink() ?>" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
                                </div>
                            </div>
                        </div>
                        <div class='triangle-box'></div>
                    </div>
                </div>
            </div>
        <?php $i++; endwhile;?>
    <?php else : ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
</div>

<?php
get_footer();

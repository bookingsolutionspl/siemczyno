<?php
/**
 * Template Name: Galeria
 */


get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section pt-6">
    <div class="container">
        <div class="gallery-main" data-categories="<?= count($fields['galeria']) ?>">
            <div class="head-box">
                <?php foreach ($fields['galeria'] as $key => $category) {
                    if (!empty($category['nazwa'])) { ?>
                        <button type="button" class="btn btn-main h7 category-button <?= $key ?> <?php if ($key == 'kategoria_1') echo 'active-btn'; ?>"><?= $category['nazwa'] ?></button>
                <?php }
                } ?>
            </div>

            <?php foreach ($fields['galeria'] as $key => $category) {
                if (!empty($category['nazwa'])) { ?>
                    <div class="item-group <?= $key ?> <?php if ($key != 'kategoria_1') echo 'd-none'; ?>">
                        <div class="title-wrapper text-center mb-4">
                            <h2 class="title-primary text-left h3"><?= $category['nazwa'] ?></h2>
                        </div>
                        <div class="gallery standard gallery-page">
                            <?php foreach ($category['zdjecia'] as $key => $img) {
                                if (!empty($img)) { ?>
                                    <div class="image entry">
                                        <?= getImage($img, true, 'glightbox'); ?>
                                    </div>
                            <?php }
                            } ?>
                        </div>
                    </div>
            <?php }
            } ?>
        </div>
    </div>
</div>

<div class="section bg-image pt-0">
    <div class="image top-0">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
        <div class="container">
            <div class="narrow narrow-sm">
                    <?php get_template_part('template-parts/form', 'contact'); ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();

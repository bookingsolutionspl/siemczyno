
<?php
/**
 * Template Name: Kontakt
 */

get_header();

$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="section pb-0">
    <div class="container">
        <div id="contact-box" class="contact-box">
            <div class="first-half">
                <div class="contact-info">
                    <div class="title-wrapper text-center mb-5">
                        <h2 class="title-primary h2"><span class="text-black"><?= __('Skontaktuj się z nami', 'siemczyno') ?></span><br><span class="color-main h3"><?= __('Zapraszamy', 'siemczyno') ?><span class="text-black"></span></span></h2>
                    </div>
                    <div class="logo-footer mb-5">
                        <div class="icon mx-auto">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </div>
                    <div class="text-center mb-4 h5-standard">
                        <h2 class="title-primary h3"><?= __('Pałac Siemczyno', 'siemczyno') ?></h2>
                        <p class="mb-0 text-black"><?= get_option('address_1') ?></p>
                        <p class="text-black mb-0"><?= get_option('address_2') ?></p>
                        <p class="text-black mb-0"><span class="text-black">NIP </span><?= get_option('nip') ?></p>
                        <p class="text-black mb-0"><?= __('Nr konta', 'siemczyno'); ?></p>
                        <p class="text-black"><span class="text-black"><?= get_option('account_number') ?></p>
                    </div>
                    <div class="text-center contact-data mb-5 mb-lg-0 h5-standard">
                        <p class="text-black mb-4 fw-bold mt-4"><?= __('Rezerwuj', 'siemczyno') ?></p>
                        <a class="text-black pb-3" href="mailto:<?= get_option('email_1'); ?>"><?= get_option('email_1'); ?></a><br>
                        <a class="text-black mb-3" href="tel:<?= get_option('phone_1'); ?>"><?= __('tel.', 'siemczyno') ?> <?= get_option('phone_1'); ?></a>
                        <br>
                        <a class="text-black mb-3" href="tel:<?= get_option('phone_2'); ?>"><?= __('tel.', 'siemczyno') ?> <?= get_option('phone_2'); ?></a><br>
                        <a class="text-black" href="tel:<?= get_option('phone_3'); ?>"><?= __('tel./fax', 'siemczyno') ?> <?= get_option('phone_3'); ?></a>
                    </div>
                </div>
            </div>
            <div class="second-half">
                <?php get_template_part('template-parts/form', 'contact'); ?>
            </div>
        </div>
    </div>
</div>

<div class="section pb-0">
    <div class="map lazy-map"></div>
</div>

<?php
get_footer();
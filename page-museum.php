<?php
/**
 * Template Name: Muzeum
 */


get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<div class="bg-image overflow-hidden">
    <div class="image">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
    <div class="section section-p-big pb-0">
        <div class="container">
            <div class="narrow">
                <div class="logo-home-page mb-5 entry">
                    <a class="homepage-link" href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                </div>
                <div class="mb-5 entry">
                    <div class="content text-center narrow h5-standard mb-5 entry">
                        <?= $fields['opis'] ?>
                    </div>
                    <div class="text-center">
                        <a href="<?= $fields['pdf'] ?>" target="_blank" class="btn btn-main">
                            <?= __('Cennik', 'siemczyno') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section-gallery scrollTo">
    <div class="container">
        <div class="title-wrapper text-center mb-5 entry">
            <h2 class="title-primary h3 mb-2"><?= $fields['galeria']['tytul'] ?></h2>
            <hr class="my-4">
        </div>         
        <div class="gallery standard">
            <?php
                if ($fields['galeria']) {
                    foreach ($fields['galeria'] as $key => $img) { 
                        if(!empty($img)) {
                            if (strpos($key, 'zdjecie_') !== false) {
                                echo '<div class="image entry">' .
                                getImage($img, 'true', 'glightbox', 'large')
                                . '</div>';
                            }
                        }
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php
get_footer();

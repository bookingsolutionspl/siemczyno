<?php
/**
 * Template Name: Pokój
 * 
 * Template Post Type: post
 */

get_header();

$fields = get_fields();
$currentPostId = get_the_ID();
?>

<?php get_template_part('template-parts/header', 'secondary');

while ( have_posts() ) : the_post();
$currentPostId = get_the_ID();
?>

<div class="section section-single-room pb-0">
    <div class="container entry">
        <div class="title-ornament mb-5">
            <h2 class="title-primary h3"><?= $fields['charakterystyka']['tytul'] ?></h2>
            <div class="separator"></div>
        </div>
        <h2 class="mb-5 p-ornament h4-standard"><?= $fields['charakterystyka']['ceny']['cena'] ?></h2>
        <div class="room-icons-box mb-5 p-ornament">

            <?php foreach ($fields['charakterystyka'] as $key => $sekcja) { if (strpos($key, 'sekcja_') !== false) { ?>
                <div class="icon">
                    <div class="image">
                        <?= getImageSvgSrc("/dist/images/icons/features/". $sekcja['ikona']) ?>
                    </div>
                    <div class="text h6">
                        <?= $sekcja['opis'] ?>
                        <?php if($sekcja['ikona'] == 'select.svg') { ?>
                            m<sup>2</sup>
                        <?php } ?>
                    </div>
                </div>
            <?php }} ?>

        </div>

        <div class="buttons-wrapper text-center text-md-start p-ornament d-flex flex-column flex-md-row justify-content-center justify-content-md-start entry">
            <button class="btn btn-main mb-3 mb-md-0 me-0 me-md-3 call-bse-widget scrollFromContact"><?= __('Rezerwuj', 'siemczyno') ?></button>
            <button type="button" class="btn btn-second scrollFrom"><?= __('Galeria', 'siemczyno') ?></button>
        </div>
    </div>
</div>

<div class="section pb-0">
    <div class="container entry">
        <div class="title-ornament mb-3">
            <h2 class="title-primary h3"><?= $fields['udogodnienia']['tytul'] ?></h2>
            <div class="separator"></div>
        </div>
        <div class="list-dashed p-ornament">
            <?php foreach ($fields['udogodnienia'] as $key => $udogodnienie) { 
                if (strpos($key, 'udogodnienie_') !== false) {
                    if(!empty($udogodnienie['nazwa'])) {?>
                        <div class="item">
                            <p class="h6">
                                <?= getImageSvgSrc("/dist/images/icons/features/". $udogodnienie['ikona']) ?>
                                <?= $udogodnienie['nazwa'] ?>
                            </p>
                        </div>
            <?php   }
                }
            } ?>
        </div>
    </div>
</div>

<div class="section pb-0">
    <div class="container entry">
        <div class="title-ornament mb-5">
            <h2 class="title-primary h3"><?= $fields['informacje']['tytul'] ?></h2>
            <div class="separator"></div>
        </div>
        <div class="additional-info h6 p-ornament">
            <?= $fields['informacje']['opis'] ?>
            <p class="mt-5"><?= __('Telefon', 'siemczyno') ?>: <a href="tel:<?= get_option('phone_1') ?>"><?= get_option('phone_1') ?></a></p>
            <p><?= __('Telefon / fax', 'siemczyno') ?>: <a href="tel:<?= get_option('phone_3') ?>"><?= get_option('phone_3') ?></a></p>
            <p><?= __('Email', 'siemczyno') ?>: <a href="mailto:<?= get_option('email_1') ?>"><?= get_option('email_1') ?></a></p>
        </div>
    </div>
</div>

<div class="section section-gallery scrollTo">
    <div class="container">
        <div class="title-wrapper text-center mb-5 entry">
            <h2 class="title-primary h3 mb-2"><?= $fields['galeria']['tytul'] ?></h2>
            <hr class="my-4">
        </div>         
        <div class="gallery standard">
            <?php
                if ($fields['galeria']) {
                    foreach ($fields['galeria'] as $key => $img) { 
                        if(!empty($img)) {
                            if (strpos($key, 'zdjecie_') !== false) {
                                echo '<div class="image entry">' .
                                getImage($img, 'true', 'glightbox', 'large')
                                . '</div>';
                            }
                        }
                    }
                }
            ?>
        </div>
    </div>
</div>

<div class="section bg-image pt-0 scrollToContact entry">
    <div class="image top-0">
        <img src="<?= get_template_directory_uri(); ?>/dist/images/bg/palace-bg.png" alt="plaża">
    </div>
        <div class="container">
            <div class="narrow narrow-sm">
                    <?php get_template_part('template-parts/form', 'contact'); ?>
            </div>
        </div>
    </div>
</div>


<?php endwhile; ?>

<?php
get_footer();
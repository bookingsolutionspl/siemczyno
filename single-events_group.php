<?php
/**
 * Template Name: Wydarzenie
 * 
 * Template Post Type: post
 */

get_header();

$fields = get_fields();
$currentPostId = get_the_ID();


$args = [
    'post_type' => 'events_group',
    'posts_per_page'=>3,
    'post__not_in' => array($currentPostId),
];

$wpb_all_query = new WP_Query($args);
$numberOfPosts = $wpb_all_query->post_count;
$i = 1;

?>

<?php get_template_part('template-parts/header', 'secondary');

while ( have_posts() ) : the_post();
$currentPostId = get_the_ID();
?>

<div class="section">
    <div class="container">
        <div class="event-wrapper entry">
            <div class="content-wrapper">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <p class="h7">
                        <span class="h2-standard day">
                            <?= date_i18n('d', strtotime($fields['data'])) ?>
                        </span>
                        <?= date_i18n('F', strtotime($fields['data'])) ?>
                    </p>

                    <?php the_content(); ?>

                </article>
            </div>
            <?php if ($wpb_all_query->have_posts()) : ?>
                <div class="sidebar-wrapper">
                    <div class="sidebar entry">
                        <div class="title-wrapper text-center mb-4">
                            <h2 class="title-primary color-main h3"><?= __('Zobacz także', 'siemczyno') ?></h2>
                        </div>
                        <div class="other-events-list">
                        <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); ?>

                            <a href="<?= get_permalink() ?>" class="event mb-3">
                                <div class="title-wrapper text-center">
                                    <p class="h5-standard"><?= the_title() ?></p>
                                </div>

                                <?php if ($i < $numberOfPosts) : ?>
                                    <hr class="hr-black">
                                <?php endif; ?>
                            </a>

                            <?php $i++; endwhile;?>

                        <div class="button-wrapper text-center">
                            <a href="<?= getTranslatedUrl('wydarzenia') ?>" class="btn btn-main"><?= __('Pokaż wszystkie', 'siemczyno') ?></a>
                        </div>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </div>
</div>

<?php endwhile; ?>

<?php
get_footer();
import {locales} from "./locale";

const locale = locales('validator');

const rules = {
    checkRequired: function (item) {
        return item.checked;
    },
    required: function (item, value) {
        if (value.length <= 0) {
            renderHint(item, 'required', locale.t('validator.required'));
            return false;
        } else {
            removeHint(item, 'required');
        }

        return true;
    },
    min: function (item, value, conditionValue) {
        if (value.length < conditionValue && value.length > 0) {
            renderHint(item, 'min', locale.t('validator.min'));
            return false;
        } else {
            removeHint(item, 'min');
        }

        return true;
    },
    max: function (item, value, conditionValue) {
        if (value.length > conditionValue) {
            renderHint(item, 'max', locale.t('validator.max', {value: conditionValue}));
            return false;
        } else {
            removeHint(item, 'max');
        }

        return true;
    },
    person: function (item, value) {
        if (!/\s/.test(value)) {
            renderHint(item, 'person', locale.t('validator.person'));
            return false;
        } else {
            removeHint(item, 'person');
        }

        return true;
    },
    postalCode: function (item, value) {
        const regExp = new RegExp("\\d{2}-\\d{3}");
        if (!regExp.test(value) && value.length > 0 || value.length > 6) {
            renderHint(item, 'postalCode', locale.t('validator.postal_code'));
            return false;
        } else {
            removeHint(item, 'postalCode');
        }

        return true;
    },
    nip: function (item, value) {
        let nipValue = value.replace(/[\\-]/gi, '').replace(/ /g, '');

        let weight = [6, 5, 7, 2, 3, 4, 5, 6, 7];
        let sum = 0;
        let controlNumber = parseInt(nipValue.substring(9, 10));
        let weightCount = weight.length;
        for (let i = 0; i < weightCount; i++) {
            sum += (parseInt(nipValue.substr(i, 1)) * weight[i]);
        }

        if (sum % 11 !== controlNumber) {
            renderHint(item, 'nip', locale.t('validator.nip'));
            return false;
        } else {
            removeHint(item, 'nip');
        }

        return true;
    },
    email: function (item, value) {
        const regExp = new RegExp("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
        if (!regExp.test(value) && value.length > 0) {
            renderHint(item, 'email', locale.t('validator.email'));
            return false;
        } else {
            removeHint(item, 'email');
        }

        return true;
    },
    phone: function (item, value) {
        const regExp = new RegExp("[ ]{2,}");
        if (regExp.test(value) && value.length > 0) {
            renderHint(item, 'phone', locale.t('validator.phone'));
            return false;
        } else {
            removeHint(item, 'phone');
        }

        return true;
    },
    minValue: function (item, value, conditionValue) {
        if (parseInt(value) < conditionValue) {
            renderHint(item, 'minValue', locale.t('validator.min_value', {value: conditionValue}));
            return false;
        } else {
            removeHint(item, 'minValue');
        }

        return true;
    },
    maxValue: function (item, value, conditionValue) {
        if (parseInt(value) > conditionValue) {
            renderHint(item, 'maxValue', locale.t('validator.max_value', {value: conditionValue}));
            return false;
        } else {
            removeHint(item, 'maxValue');
        }

        return true;
    },
    pesel: function (item, value) {
        if (value.length > 0) {
            if (!/^[0-9]{11}$/.test(value)) {
                renderHint(item, 'pesel', locale.t('validator.pesel_type_correct'));
                return false;
            }
            const times = [1, 3, 7, 9];
            const digits = `${value}`.split('').map(digit => parseInt(digit, 10));
            const dig11 = digits.splice(-1)[0];
            let control = digits.reduce((previousValue, currentValue, index) => previousValue + currentValue * times[index % 4]) % 10;

            if (!(10 - (control === 0 ? 10 : control) === dig11)) {
                renderHint(item, 'pesel', locale.t('validator.pesel_incorrect'));
                return false;
            } else {
                removeHint(item, 'pesel');
            }
        } else {
            removeHint(item, 'pesel');
        }

        return true;
    }
};

export function validatorRegister(item) {
    item.querySelectorAll('.validate').forEach((item) => {
        const validators = item.getAttribute('data-validate').split('|');

        validators.forEach((validator) => {
            validator = validator.split(':');

            if (typeof rules[validator[0]] === "function") {
                const input = item.querySelectorAll('input');

                if (input.length > 0) {
                    const inputType = input[0].getAttribute('type');

                    switch (inputType) {
                        case 'checkbox' :
                            validateCheckbox(input, item);
                            break;
                        case 'radio' :
                            validateCheckbox(input, item);
                            break;
                        default :
                            validateText(input[0], item, validator);
                    }
                } else {
                    const textarea = item.querySelector('textarea');

                    if (textarea) {
                        validateText(textarea, item, validator);
                    }
                }
            } else {
                console.info(`Cannot validate field. Function ${validate[0]}() doesn't exist,`);
            }
        });
    });
}

export function isValid(item) {
    let formValid = true;
    const validateFieldsAll = item.querySelectorAll('.validate');
    const validateFieldsDiff = item.querySelectorAll('div.condshow.hidden .validate');
    const validateFields = Object.values(validateFieldsAll).filter(x => !Object.values(validateFieldsDiff).includes(x));

    validateFields.forEach((item) => {
        const validators = item.getAttribute('data-validate').split('|');

        validators.forEach((validator) => {
            validator = validator.split(':');

            if (typeof rules[validator[0]] === "function") {
                const input = item.querySelectorAll('input');

                if (input.length > 0) {
                    const inputType = input[0].getAttribute('type');

                    switch (inputType) {
                        case 'checkbox' :
                            if (!validateCheckboxIns(input, item)) {
                                formValid = false;
                            }
                            break;
                        case 'radio' :
                            if (!validateCheckboxIns(input, item)) {
                                formValid = false;
                            }
                            break;
                        default :
                            if (!validateTextIns(input[0], item, validator)) {
                                formValid = false;
                            }
                    }
                } else {
                    const textarea = item.querySelector('textarea');

                    if (textarea) {
                        if (!validateTextIns(textarea, item, validator)) {
                            formValid = false;
                        }
                    }
                }
            } else {
                console.info(`Cannot validate field. Function ${validate[0]}() doesn't exist,`);
            }
        });
    });

    return formValid;
}

function validateTextIns(input, item, validator) {
    if (validator[1]) {
        return rules[validator[0]](item, input.value, validator[1]);
    }

    return rules[validator[0]](item, input.value);
}

function validateCheckboxIns(input, item) {
    let isValid = false;

    input.forEach((input) => {
        const t = rules['checkRequired'](input, input.value);

        if (t) {
            isValid = true;
        }
    });

    if (!isValid) {
        renderHint(item, 'checkRequired', 'To pole jest wymagane.');
    } else {
        removeHint(item, 'checkRequired');
    }

    return isValid;
}

function validateText(input, item, validator) {
    let typingTimer = 0;
    const typingInterval = 1000;

    input.addEventListener('keyup', () => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => {
            if (!validator[1]) {
                rules[validator[0]](item, input.value);
            } else {
                rules[validator[0]](item, input.value, validator[1]);
            }
        }, typingInterval);
    });
}

function validateCheckbox(input, item) {
    let isValid = true;

    input.forEach((input) => {
        input.addEventListener('click', () => {
            isValid = rules['checkRequired'](item);

            if (isValid) {
                renderHint(item, 'checkRequired', 'To pole jest wymagane.');
            } else {
                removeHint(item, 'checkRequired');
            }
        });
    });
}

function renderHint(item, fnName, message) {
    removeHint(item, fnName);
    if (item !== null) {
        item.classList.remove('valid');
        item.classList.add('invalid');

        if (item.classList.contains('checkbox')) {
            let checkmark = item.querySelector('.checkmark');
            checkmark.classList.add('checkmark-invalid');
        } else {
            item.insertAdjacentHTML('beforeend', `<div class="validate-hint ${fnName}"><span class="message">${message}</span></div>`);
        }
    }
}

function removeHint(item, fnName) {
    if (item !== null) {
        item.querySelectorAll(`.validate-hint.${fnName}`).forEach((item) => {
            item.remove();
        });

        if (item.querySelectorAll('.validate-hint').length === 0) {
            item.classList.add('valid');
            item.classList.remove('invalid');
        }
    }
}

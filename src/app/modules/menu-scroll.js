module.exports.menuScroll = () => {
    let didScroll = false;
    let isHidden = true;

    window.onscroll = function (e) {
        didScroll = true;
    }

    setInterval(function () {
        if (didScroll) {
            scrolled();
            didScroll = false;
            isHidden = false;
            hide_event_wrapper();
        }
    }, 250);

    function scrolled() {
        const scrollPosition = window.scrollY;
        const navBar = document.querySelector('nav.navbar');
        const ctaBox = document.querySelector('.cta-box');

        if (scrollPosition > 300) {
            navBar.classList.add('nav-float')
            navBar.classList.remove('nav-hide');
            ctaBox.classList.add('cta-box-float');
        } else {
            if (isHidden) {
                navBar.classList.add('nav-hide');
                setTimeout(function(){
                    navBar.classList.remove('nav-hide');
                }, 200);
                isHidden = false;
            }

            navBar.classList.remove('nav-float');
            ctaBox.classList.remove('cta-box-float');
        }
    }

    function hide_event_wrapper() {
        if (document.querySelector('.splide-event-wrapper'))
        {
            let eventWrapper = document.querySelector('.splide-event-wrapper');
            eventWrapper.classList.add('event-hide-element');
        }
    }
}
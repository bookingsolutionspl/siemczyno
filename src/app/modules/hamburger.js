module.exports.hamburgerAnimate = () => {
    const navbarToggler = document.querySelector('.navbar-toggler');
    const hamburgerMenu = document.querySelector('.hamburger-menu');

    navbarToggler.addEventListener('click', event => {
        hamburgerMenu.classList.toggle("animate");
    });
};
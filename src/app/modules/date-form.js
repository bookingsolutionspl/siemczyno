if (document.getElementById('date-form')) {
    document.querySelectorAll('#date-form').forEach(form => {
        form.querySelector('button[type=submit]').addEventListener('click', button => {
            let date = document.getElementById('dateRange');
            localStorage.setItem('dateRange', date.value);
        });
    });
}

if (document.getElementById('dateRange') != null && localStorage.getItem('dateRange') != null) {
    document.getElementById('dateRange').value = localStorage.getItem('dateRange');
}
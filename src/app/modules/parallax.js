module.exports.parallax = () => {
    let parallaxImages = document.querySelectorAll('.img-parallax');

    parallaxImages.forEach(image => {
        let imgParent = image.parentElement;

        function parallaxImg () {
            let speed = image.dataset.speed; // From 0 to 1
            let imgY = imgParent.getBoundingClientRect().top;
            let winY = image.scrollTop;

            let winH = image.height;
            let parentH = imgParent.offsetHeight;
        
            // The next pixel to show on screen      
            let winBottom = winY + winH;

            // If block is shown on screen
            if (winBottom > imgY && winY < imgY + parentH) {
                // Number of pixels shown after block appear
                var imgBottom = ((winBottom - imgY) * speed);
                // Max number of pixels until block disappear
                var imgTop = winH + parentH;
                // Porcentage between start showing until disappearing
                var imgPercent = ((imgBottom / imgTop) * 100) + (50 - (speed * 50));
            }

            let positionStyle = 'top:' + imgPercent + '%;' + 'transform: translate(-50%, -' + imgPercent + '%);';
            image.setAttribute('style', positionStyle);
        }

        document.addEventListener('scroll', function(e) {
            parallaxImg();
        });
        parallaxImg();
    });
}
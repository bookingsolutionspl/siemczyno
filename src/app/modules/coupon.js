module.exports.coupon = () => {
    const coupon = document.querySelector('#coupon');
    const cancelBtn = document.querySelector('.coupon-wrapper .cancel');

    setTimeout(function(){
        const cancel = document.querySelector('.coupon-wrapper .cancel');
        const infoBox = document.querySelector('.coupon-wrapper .info-box');
        const icon = document.querySelector('.coupon-wrapper > .icon');

        icon.classList.add('pe-auto');
        coupon.classList.toggle('active');
        cancel.classList.toggle('d-none');
        infoBox.classList.toggle('d-none');
    }, 5000);

    coupon.addEventListener('click', function(e) {
        const cancel = this.nextElementSibling;
        const infoBox = cancel.nextElementSibling;

        this.classList.toggle('active');
        cancel.classList.toggle('d-none');
        infoBox.classList.toggle('d-none');
    }, false);

    cancelBtn.addEventListener('click', function(e) {
        this.classList.toggle('d-none');
        this.nextElementSibling.classList.toggle('d-none');
        this.previousElementSibling.classList.toggle('active');
    }, false);
}
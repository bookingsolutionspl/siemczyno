import Splide from '@splidejs/splide';

export function slider() {


    new Splide( '#splideHeader', {
        type: 'slide',
        perPage: 1,
        perMove: 1,
        autoplay: true,
        speed: 800,
        pagination: true,
        arrows: false,
    }).mount();
}
import Polyglot from "node-polyglot";

export function locales(slug) {
    const polyglot = new Polyglot();
    const translations = require(`../translations/${document.documentElement.lang}/${slug}.json`);

    polyglot.extend(translations);
    return polyglot;
}
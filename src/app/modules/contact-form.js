import {isValid, validatorRegister} from "./validator";

if (document.getElementById('contact-form')) {
    document.querySelector('#contact-form button[type=submit]').addEventListener('click', event => {
        event.preventDefault();
        let contactForm = document.getElementById('contact-form');
        const spinner = document.querySelector('.spinner-border');
        const messageSuccess = document.querySelector('.message-success');
        const messageFail = document.querySelector('.message-fail');

        if(isValid(contactForm)) {
            let formData = new FormData(contactForm);

            async function postData(url='', data={}) {
                const response = await fetch(
                    url,
                    {
                        method: "POST",
                        mode: "same-origin",
                        cache: 'no-cache',
                        credentials: "same-origin",
                        redirect: 'follow',
                        referrerPolicy: 'no-referrer',
                        body: formData,
                    },
                );
            
                return response.status;
            }
            
            function handleResponse(message) {
                spinner.classList.add('d-none');
                message.classList.remove('d-none');
                contactForm.reset();
            }

            postData('/wp-content/themes/siemczyno/send-email.php', formData)
                .then(response => {
                    // Manipulate response here
                    if (response === 200) {
                        handleResponse(messageSuccess);
                    } else {
                        handleResponse(messageFail);
                    }
                });
                spinner.classList.remove('d-none');
        }
        validatorRegister(contactForm);
    });
}
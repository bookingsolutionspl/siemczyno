import Splide from '@splidejs/splide';

export function sliderEvents() {

    new Splide( '#splideEvents', {
        type: 'loop',
        perPage: 1,
        perMove: 1,
        // autoplay: true,
        speed: 800,
        pagination: false,
        arrows: true,
    }).mount();
}
module.exports.lazyLoading = () => {
    var itemsToEntry = [].slice.call(document.querySelectorAll(".entry"));

    if ("IntersectionObserver" in window) {
        var itemEntryObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var itemToEntry = entry.target;
                    itemToEntry.classList.remove("entry");
                    itemToEntry.classList.add("entered");
                    itemEntryObserver.unobserve(itemToEntry);
                }
            });
        });

        itemsToEntry.forEach(function (itemToEntry) {
            itemEntryObserver.observe(itemToEntry);
        });
    } else {
        itemsToEntry.forEach(function (itemToEntry) {
            itemToEntry.classList.remove("entry");
            itemToEntry.classList.add("entered");
        });
    }

    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

    if ("IntersectionObserver" in window) {
        var lazyImageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                var lazyImage = entry.target;

                if (entry.isIntersecting && lazyImage.classList.contains('lazy')) {
                    if (lazyImage.dataset.src) {
                        lazyImage.src = lazyImage.dataset.src;
                    } else {
                        lazyImage.srcset = lazyImage.dataset.srcset;
                    }
                    lazyImageObserver.unobserve(lazyImage);
                    lazyImage.classList.remove('lazy');
                }
            });
        }, {rootMargin: '100px 0px 100px 0px'});

        lazyImages.forEach(function (lazyImage) {
            lazyImageObserver.observe(lazyImage);
        });
    } else {
        lazyImages.forEach(function (lazyImage) {
            if (lazyImage.dataset.src) {
                lazyImage.src = lazyImage.dataset.src;
            } else {
                lazyImage.srcset = lazyImage.dataset.srcset;
            }
        });
    }

    var lazyBg = [].slice.call(document.querySelectorAll(".lazy-bg"));

    if ("IntersectionObserver" in window) {
        var lazyImageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var lazyImage = entry.target;
                    lazyImage.style = lazyImage.dataset.style;
                    lazyImageObserver.unobserve(lazyImage);
                }
            });
        }, {rootMargin: '100px 0px 100px 0px'});

        lazyBg.forEach(function (lazyImage) {
            lazyImageObserver.observe(lazyImage);
        });
    } else {
        lazyBg.forEach(function (lazyImage) {
            lazyImage.style = lazyImage.dataset.style;
        });
    }

    // Map lazy loading
    let mapContainer = document.querySelector(".map");

    function appendMapSrc() {
        if (typeof google === 'object' && typeof google.maps === 'object') {
            initMap();
        } else {
            let mapsJS = document.createElement('script');
            mapsJS.setAttribute('async', '');
            mapsJS.setAttribute('defer', '');
            mapsJS.src = 'https://maps.googleapis.com/maps/api/js?key=' + 'AIzaSyBwQmjBp85aHXYrjOnUdXJoB_a0xtuR_78' + '&callback=initMap';
            document.getElementsByTagName('head')[0].appendChild(mapsJS);
        }
    }

    if(mapContainer) {
        if ("IntersectionObserver" in window) {
            var lazyMapObserver = new IntersectionObserver(function (entries, observer) {
                entries.forEach(function (entry) {
                    if (entry.isIntersecting) {
                        appendMapSrc();
                        lazyMapObserver.unobserve(mapContainer);
                    }
                });
            });
            
            lazyMapObserver.observe(mapContainer);
        } else {
            appendMapSrc();
        }
    }
    //----

    //Lazy videos loading
    var lazyVideos = [].slice.call(document.querySelectorAll(".lazy-video"));

    if ("IntersectionObserver" in window) {
        var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var lazyVideo = entry.target.children[0];
                    lazyVideo.src = lazyVideo.dataset.src;
                    entry.target.load();
                    lazyVideo.classList.remove("lazy-video");
                    lazyVideoObserver.unobserve(lazyVideo);
                }
            });
        });

        lazyVideos.forEach(function (lazyVideo) {
            lazyVideoObserver.observe(lazyVideo);
        });
    } else {
        lazyVideos.forEach(function (lazyVideo) {
            lazyVideo.src = lazyVideo.dataset.src;
            lazyVideo.classList.remove("lazy-video");
        });
    }
    //----
};
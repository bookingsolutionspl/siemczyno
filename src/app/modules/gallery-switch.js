module.exports.gallerySwitch = () => {
    const categoryButtons = document.querySelectorAll('.category-button');
    const categoriesNumber = document.querySelector('.gallery-main').dataset.categories;
    const itemGroups = document.querySelectorAll('.item-group');

    categoryButtons.forEach((button, index) => {
        let currentBtnIndex = index + 1;
        button.addEventListener('click', function(e) {
            itemGroups.forEach(group => {
                group.classList.add('d-none');
                if (group.classList.contains('kategoria_' + currentBtnIndex)) group.classList.remove('d-none');
            });

            categoryButtons.forEach(button => {
                button.classList.remove('active-btn');
            })
            
            this.classList.add('active-btn');
        });
    });
}
const bootstrap = require('bootstrap');
import GLightbox from 'glightbox';
import {lazyLoading} from './modules/lazyload';
import {hamburgerAnimate} from './modules/hamburger';
import {menuScroll} from './modules/menu-scroll';
import {coupon} from './modules/coupon';
import {gallerySwitch} from './modules/gallery-switch';
import {contactForm} from './modules/contact-form';
import {parallax} from './modules/parallax';
import {dateForm} from './modules/date-form';
import Litepicker from 'litepicker';
import {slider} from './modules/slider';
import {sliderEvents} from './modules/slider-events';

const lightbox = GLightbox({
    touchNavigation: true,
    loop: true,
    autoplayVideos: false,
});

lazyLoading();
hamburgerAnimate();
menuScroll();

if (document.querySelector('.splide')) slider();
if (document.querySelector('#coupon')) coupon();
if (document.querySelectorAll('.img-parallax')) parallax();
if (document.querySelector('.gallery-main')) gallerySwitch();
if (document.querySelector('#splideEvents')) sliderEvents();

function scrollToElement(elem, offset = 0) {
    const y = elem.getBoundingClientRect().top + window.pageYOffset + offset;
    window.scrollTo({top: y, behavior: 'smooth'});
}

if (document.querySelector('.scrollFrom')) {
    const scrollFrom = document.querySelector('.scrollFrom');
    
    scrollFrom.addEventListener('click', function(e) {
        scrollToElement(document.querySelector('.scrollTo'), 0);
    }, false);
}

if (document.querySelector('.scrollFromContact')) {
    const scrollFrom = document.querySelector('.scrollFromContact');
    
    scrollFrom.addEventListener('click', function(e) {
        scrollToElement(document.querySelector('.scrollToContact'), 0);
    }, false);
}

const lang = document.documentElement.lang;

if (document.getElementById('dateRange') != null) {
    const picker = new Litepicker({
        element: document.getElementById('dateRange'),
        lang: lang,
        singleMode: false,
    });
}
<?php
/**
 * Template Name: Hss
 */


get_header();
$fields = get_fields();
?>

<?php get_template_part('template-parts/header', 'primary'); ?>

<?php
    $i = 1;
    foreach ($fields['sekcje'] as $sekcja) {
        if(empty($sekcja['tekst'])){
            continue;
        }

        $modulo = $i % 2;
        
?>

<div class="section bg-white <?= $modulo ? '' : 'pt-0' ?>">
    <div class="container hss-container">
        <div class="<?= $modulo ? 'hss-box' : 'hss-box-revert' ?>">
            <div class="image-wrapper">
                <div class="image">
                    <?= getImage($sekcja['zdjecie_1'], 'true') ?>
                </div>
                <div class="image">
                    <?= getImage($sekcja['zdjecie_2'], 'true') ?>
                </div>
            </div>
            <div class="content h5-standard">
                <?= $sekcja['tekst'] ?>
            </div>
            <div class="button-wrapper text-center">
                <a href="<?= $fields['link'] ?>" target="_blank" class="btn btn-main"><?= __('Czytaj więcej', 'siemczyno') ?></a>
            </div>
        </div>
    </div>
</div>
<?php $i++; } ?>

<?php
get_footer();

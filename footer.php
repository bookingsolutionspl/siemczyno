<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
</main>

<!-- <div id="bse-widget-frame" class="bse-close-widget"></div> -->

<footer>
    <div class="container">
        <div class="footer-box">
            <div class="left-content-box">
                <div class="logo-footer">
                    <a href="<?php echo esc_url(home_url('')); ?>/">
                        <div class="icon">
                            <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo.png" alt="<?= get_option('name') ?>">
                        </div>
                    </a>
                    <div class="socials mt-3">
                        <a rel="nofollow" href="<?= get_option('facebook') ?>" target="_blank">
                            <div class="icon fb me-3">
                                <?= getImageSvgSrc("/dist/images/icons/socials/facebook.svg") ?>
                            </div>
                        </a>
                        <a rel="nofollow" href="<?= get_option('instagram') ?>" target="_blank">
                            <div class="icon insta">
                                <?= getImageSvgSrc("/dist/images/icons/socials/instagram.svg") ?>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="footer-adress-box">
                    <p class="h5 title-primary amazone-regular text-white mb-2"><?= __('Pałac Siemczyno', 'siemczyno') ?></p>
                    <div class="footer-adress">
                    <div class="icon">
                        <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/contact/marker-outline.svg" alt="">
                    </div>
                    <div>
                        
                        <p class="street text-white h7">ul. <?= get_option('address_1') ?></p>
                        <p class="city text-white h7"><?= get_option('address_2') ?></p>
                        <p class="city text-white h7">NIP <?= get_option('nip') ?></p>
                    </div>
                </div>
                </div>

                
                <div class="footer-contact">
                    <p class="label-contact text-white text-uppercase h6"><?= __('Kontakt', 'siemczyno') ?></p>
                    <div class="email">
                        <div class="icon">
                            <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/contact/envelope-outline.svg" alt="">
                        </div>
                        <a href="mailto:<?= get_option('email_1') ?>" class="text-white text-decoration-none h7"><?= get_option('email_1') ?></a>
                    </div>
                    <hr class="hr-light my-3">
                    <div class="phone mb-lg-0">
                        <div class="icon">
                            <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/contact/phone-outline.svg" alt="">
                        </div>
                        <div class="phones">
                            <a href="tel:<?= get_option('phone_1') ?>" class="text-white text-decoration-none h7"> <?= get_option('phone_1') ?></a>
                        </div>
                    </div>
                    <div class="phone">
                        <div class="icon">
                            <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/contact/phone-outline.svg" alt="">
                        </div>
                        <div class="phones">
                            <a href="tel:<?= get_option('phone_2') ?>" class="text-white text-decoration-none h7"> <?= get_option('phone_2') ?></a>
                        </div>
                    </div>
                    <div class="phone">
                        <div class="icon">
                            <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/contact/phone-outline.svg" alt="">
                        </div>
                        <div class="phones">
                            <a href="tel:<?= get_option('phone_3') ?>" class="text-white text-decoration-none h7"> <?= get_option('phone_3') ?></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box right-content-box">
                <div class="footer-menu">
                    <?php
                    wp_nav_menu(array(
                        'menu' => 'navigation-footer',
                        'menu_class' => 'nav navbar-nav',
                        'items_wrap' => '<ul class="navbar-footer">%3$s</ul>',
                        'theme_location' => 'navigation-footer',
                        'depth' => 0,
                        'walker' => new BootstrapNavMenuWalker()
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <hr class="hr-light d-none d-xl-block mt-5 mb-3">
        <div class="realization-box mt-4 mt-xl-0 pb-0 pb-xl-5">
            <div class="realization-wrapper">
                <p class="copyright text-white h8 mb-0">© 2021 <?= get_option('name') ?>.</p>
                <p class="copyright text-white h8 mb-0"><?= __('Wszystkie prawa zastrzeżone', 'siemczyno') ?>.</p>

            </div>
            <div class="realization-wrapper">
                <div class="flags">
                    <a href="<?= getTranslatedUrl('dotacje-unijne') ?>" class="icon">
                        <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/flags/european-union.png" alt="unia europejska">
                    </a>
                    <a href="<?= getTranslatedUrl('subwencja-pfr') ?>" class="icon">
                        <img class="lazy" data-src="<?= get_template_directory_uri(); ?>/dist/images/icons/others/polski-fundusz-rozwoju-logo.svg" alt="polski fundusz rozwoju">
                    </a>
                </div>
            </div>
            <div class="realization-wrapper">
                <div class="createdby">
                    <p class="h8 text-white mb-0"><?= __('Realizacja', 'siemczyno') ?></p>
                    <a href="https://bookingsolutions.pl/" target="_blank" class="booking-solutions-logo" rel="nofollow">
                        <img data-src="<?= get_template_directory_uri(); ?>/dist/images/identify/logo-booking-solutions-footer.svg" class="booking-solutions-logo lazy" alt="booking solutions">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('template-parts/media-widget', ''); ?>
</footer>

<div class="cta-box cta-hide">
    <div class="cta-button-wrapper">
        <a href="tel:<?= get_option('phone_1') ?>" class="btn btn-second h7 bse-call"><?= __('Zadzwoń', 'siemczyno') ?></a>
    </div>
    <div class="cta-button-wrapper">
        <a href="<?= getTranslatedUrl('kontakt') ?>" class="btn btn-main h7"><?= __('Rezerwuj', 'siemczyno') ?></a>
    </div>
    <div class="cta-button-wrapper">
        <a href="<?= get_option('map') ?>" target="_blank" class="btn btn-second h7"><?= __('Dojazd', 'siemczyno') ?></a>
    </div>
</div>
<?php wp_footer(); ?>

<script>
    function initMap() {
        // The location of spot
        var spot = { lat: 53.556220, lng: 16.131650 };
        // The map, centered at spot
        var map = new google.maps.Map(document.querySelector(".map"), {
        zoom: 14,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#faba28"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fde293"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#9cc0f9"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ],
        center: spot,
        });

        // Popup window on click
        var contentString = `
            <strong>Pałac Siemczyno</strong><br>
            ul. Siemczyno 81<br>
            78-551 Siemczyno<br>
            <a rel="nofollow" href="https://www.google.com/maps/place/Pa%C5%82ac+Siemczyno/@53.5571544,16.1281084,15.5z/data=!4m8!3m7!1s0x46fda14000000043:0xc88c5e51b0c73881!5m2!4m1!1i2!8m2!3d53.556211!4d16.131642?hl=pl-PL" target="_blank"> <?= __('Zobacz w Mapach Google', 'siemczyno') ?></a>
        `;

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
        });

        // Hotel
        var marker = new google.maps.Marker({
        position: { lat: 53.556220, lng: 16.131650 },
        icon: {
            url: '<?= get_template_directory_uri() ?>/dist/images/identify/marker.png',
        },
        map: map,
        });

        marker.addListener("click", () => {
            infowindow.open(map, marker);
        });
    }
</script>

</body>
</html>
